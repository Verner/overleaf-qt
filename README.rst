##########################
libOverLeaf-qt
##########################

A simple Qt library for interacting with the `OverLeaf <https://www.overleaf.com>`_ web service.

Dependencies
============
  - Qt >= 5.5 (including webengine)
  - git (runtime dependency for cloning overleaf projects)
  - CMake


Installation
============

.. code-block:: bash

    $ git clone git@gitlab.com:Verner/overleaf-qt.git
    $ mkdir build
    $ cd build
    $ cmake ../overleaf-qt
    $ make -j4
    


Using the app
=============

.. code-block:: bash

    $ overleaf login
    $ overleaf list_projects
    $ overleaf clone PROJECT_ID


LICENSE
=======


Copyright 2017  Jonathan Verner <jonathan@temno.eu>

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 2 of
the License or (at your option) version 3 or any later version
accepted by the membership of KDE e.V. (or its successor approved
by the membership of KDE e.V.), which shall act as a proxy
defined in Section 14 of version 3 of the license.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see http://www.gnu.org/licenses/.
