/*
 * A Qt library for interacting with the OverLeaf web service.
 * Copyright 2017  Jonathan Verner <jonathan@temno.eu>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version
 * accepted by the membership of KDE e.V. (or its successor approved
 * by the membership of KDE e.V.), which shall act as a proxy
 * defined in Section 14 of version 3 of the license.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "api.h"
#include "project.h"

#include "api_p.h"
#include "logindialog_p.h"

#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QVariant>
#include <QWebEnginePage>
#include <QWebEngineProfile>
#include <QWidget>

OverLeaf::ScriptAction::ScriptAction(const QUrl& p_url, const QString& p_script):
    AgentAction(), url(p_url), script(p_script)
{
}

void OverLeaf::ScriptAction::start(QWebEnginePage *agent)
{
    agent->load(url);
}

void OverLeaf::ScriptAction::onAgentfinished(QWebEnginePage *agent)
{
    agent->runJavaScript(script, [this](const QVariant &v){finished(v); deleteLater();});
}

OverLeaf::AJAXAction::AJAXAction(const QUrl& p_url, QWebEngineHttpRequest::Method p_method):
    AgentAction(), method(p_method), url(p_url)
{
}

void OverLeaf::AJAXAction::addData(const QString& name, const QString& val)
{
    postData[name] = val;
}

void OverLeaf::AJAXAction::setHeader(const QString& name, const QString& val)
{
    headers[name] = val;
}


void OverLeaf::AJAXAction::start(QWebEnginePage* agent)
{
    qDebug() << "Starting AJAX action at" << url;
    QWebEngineHttpRequest req(url);
    if (method == QWebEngineHttpRequest::Method::Post) {
        req = QWebEngineHttpRequest::postRequest(url, postData);
    }
    for(const auto &key : headers.keys()) {
        req.setHeader(key.toLocal8Bit(), headers[key].toLocal8Bit());
    }
    agent->load(req);
}

void OverLeaf::AJAXAction::onAgentfinished(QWebEnginePage* agent)
{
    qDebug() << "Agent finished with AJAX action at" << url;
    qDebug() << "Getting result";
    agent->toPlainText([this](const QString& content){
        qDebug() << "Done getting result";
        finished(QVariant(content)); 
        deleteLater();
    });
}


OverLeaf::APIPrivate::APIPrivate(OverLeaf::API *p_api, QWebEngineProfile *prof, QWidget *parent): 
  agent(new QWebEnginePage(prof)),
  api(p_api),
  profile(prof), 
  loginDlg(new OverLeaf::LoginDialog(prof, parent)),
  running(false)
{
    connect(agent, &QWebEnginePage::loadFinished, this, &OverLeaf::APIPrivate::agentFinished);
    connect(loginDlg, &OverLeaf::LoginDialog::authToken, this, &OverLeaf::APIPrivate::setAuthToken);
};

OverLeaf::APIPrivate::~APIPrivate()
{
    delete loginDlg;
}

void OverLeaf::APIPrivate::agentFinished()
{
    pendingActions.first()->onAgentfinished(agent);
}

void OverLeaf::APIPrivate::actionFinished()
{
    OverLeaf::AgentAction *act = pendingActions.first();
    act->disconnect(SIGNAL(finished(const QVariant&)), this, 0);
    pendingActions.pop_front();
    runNextAction();
}

void OverLeaf::APIPrivate::runNextAction()
{
    if (pendingActions.size() == 0) {
        running = false;
        return;
    } else running = true;
    qDebug() << "Running next Action";
    OverLeaf::AgentAction *act = pendingActions.first();
    act->start(agent);
    connect(act, &OverLeaf::AgentAction::finished, this, &OverLeaf::APIPrivate::actionFinished);
}

void OverLeaf::APIPrivate::runAction(OverLeaf::AgentAction* action)
{
    qDebug() << "RunAction called";
    pendingActions << action;
    qDebug() << "Running: " << running;
    qDebug() << "Pending Actions:" << pendingActions.size();
    if (! running ) runNextAction();
}


void OverLeaf::APIPrivate::loadProjects()
{
    OverLeaf::AJAXAction *act = new AJAXAction(OverLeaf::api_docs_endpoint);
    connect(act, &OverLeaf::AJAXAction::finished, this, &OverLeaf::APIPrivate::doneLoadingProjects);
    runAction(act);
}

void OverLeaf::APIPrivate::doneLoadingProjects(const QVariant& result)
{
    QJsonObject response = QJsonDocument::fromJson(result.toByteArray()).object();
    qDebug() << "Docs Total: " << response["docs"].toArray().size();
    for( const auto& doc: response["docs"].toArray() ) {
        qDebug() << "  Loading " << doc.toObject()["title"];
        projects << new OverLeaf::Project(doc.toObject(), api);
    };
    projectsLoaded();
}

void OverLeaf::APIPrivate::setAuthToken(const QString& p_authToken)
{
    authToken = p_authToken;
}



OverLeaf::API::API(QWebEngineProfile* prof, QWidget *parent):
    d_ptr(new OverLeaf::APIPrivate(this, prof, parent))
{
    connect(d_ptr, &OverLeaf::APIPrivate::projectsLoaded, this, &OverLeaf::API::projectsLoaded);
}

OverLeaf::API::~API()
{
    delete d_ptr;
}

bool OverLeaf::API::login()
{
    return (d_ptr->loginDlg->exec() == QDialog::Accepted);
}

void OverLeaf::API::loadProjects()
{
    d_ptr->loadProjects();
}

QString OverLeaf::API::authToken()
{
    return d_ptr->authToken;
}


QList<OverLeaf::Project *> OverLeaf::API::projects()
{
    return d_ptr->projects;
}

OverLeaf::Project * OverLeaf::API::findProjectById(QString id)
{
    for(const auto& project : d_ptr->projects) {
        if (project->id() == id) return project;
    }
    return 0;
}



