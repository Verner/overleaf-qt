/*
 * A Qt library for interacting with the OverLeaf web service.
 * Copyright 2017  Jonathan Verner <jonathan@temno.eu>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version
 * accepted by the membership of KDE e.V. (or its successor approved
 * by the membership of KDE e.V.), which shall act as a proxy
 * defined in Section 14 of version 3 of the license.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef API_P_H
#define API_P_H

#include <QList>
#include <QMap>
#include <QObject>
#include <QString>
#include <QUrl>
#include <QWebEngineHttpRequest>

class QWebEnginePage;
class QWebEngineProfile;
class QWidget;

namespace OverLeaf {

    class Project;
    class LoginDialog;
    class API;
    
    const QString overleaf_server    = QStringLiteral("overleaf.com");
    const QString auth_cookie_domain = "www."+overleaf_server;
    const QString auth_cookie_name   = QStringLiteral("remember_user_token");
    const QString git_endpoint       = "https://git."+overleaf_server;
    const QString api_endpoint       = "https://www."+overleaf_server+QStringLiteral("/api/v0/current_user");
    const QUrl    api_docs_endpoint  = QUrl(api_endpoint+"/docs");
    const QUrl    api_login_endpoint = QUrl("https://www."+overleaf_server+"/users/sign_in");
      
    class AgentAction : public QObject {
        Q_OBJECT
    
    public:
        virtual void start(QWebEnginePage *agent) = 0;
    
    public slots:
        virtual void onAgentfinished(QWebEnginePage *agent) = 0;
    
    signals:
        void finished(const QVariant &result);
    };
    
    class AJAXAction : public AgentAction {
        Q_OBJECT
        
    public:
        AJAXAction(const QUrl &url, QWebEngineHttpRequest::Method method = QWebEngineHttpRequest::Method::Get);
        
        void addData(const QString &name, const QString &val);
        void setHeader(const QString &name, const QString &val);
        
        virtual void start(QWebEnginePage *agent);
    
    public slots:
        virtual void onAgentfinished(QWebEnginePage *agent);
        
    private:
        QWebEngineHttpRequest::Method method;
        QMap<QString, QString> postData, headers;
        QUrl url;
        
    };
    
    class ScriptAction : public AgentAction {
        Q_OBJECT
        
    public:
        ScriptAction(const QUrl &url, const QString &script);
        virtual void start(QWebEnginePage *agent);
        
    public slots:
        virtual void onAgentfinished(QWebEnginePage *agent);
        
    private:
        QUrl url;
        QString script;
    };
    
    class APIPrivate : public QObject {
        Q_OBJECT
    public:
        APIPrivate(OverLeaf::API *api, QWebEngineProfile *profile, QWidget *parent = 0);
        ~APIPrivate();
        
        void loadProjects();
        void runAction(AgentAction *action);
        
        QWebEnginePage *agent;
        OverLeaf::API *api;
        QWebEngineProfile *profile;
        QList<Project *> projects;
        QList<AgentAction *> pendingActions;
        LoginDialog *loginDlg;
        QString authToken;
        
    signals:
        void projectsLoaded();
        
    private:
        bool running;
        void runNextAction();

    private slots:
        void agentFinished();
        void actionFinished();
        void setAuthToken(const QString& authToken);
        
        void doneLoadingProjects(const QVariant& result);
        
    };
}

#endif // API_P_H
