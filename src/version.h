#ifndef VERSION_H
#define VERSION_H

#include <QString>

namespace OverLeaf {
    extern const QString GIT_HASH;
}

#endif // VERSION_H
