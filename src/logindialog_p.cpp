/*
 * A Qt library for interacting with the OverLeaf web service.
 * Copyright 2017  Jonathan Verner <jonathan@temno.eu>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version
 * accepted by the membership of KDE e.V. (or its successor approved
 * by the membership of KDE e.V.), which shall act as a proxy
 * defined in Section 14 of version 3 of the license.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "logindialog_p.h"

#include "api_p.h"

#include <QDebug>
#include <QFile>
#include <QNetworkCookie>
#include <QWebEngineCookieStore>
#include <QWebEnginePage>
#include <QWebEngineProfile>
#include <QWebEngineView>


OverLeaf::LoginDialog::LoginDialog(QWebEngineProfile *prof, QWidget *parent) :
    QDialog(parent), profile(prof)
{
    webView = new QWebEngineView(this);
    page = new QWebEnginePage(profile, webView);
    webView->setPage(page);
    connect(page, SIGNAL(loadFinished(bool)), this, SLOT(hideScrollBar()));
    connect(profile->cookieStore(), SIGNAL(cookieAdded(const QNetworkCookie)), this, SLOT(onCookie(const QNetworkCookie &)));
}

int OverLeaf::LoginDialog::exec()
{
    webView->show();
    page->load(OverLeaf::api_login_endpoint);
    return QDialog::exec();
}


OverLeaf::LoginDialog::~LoginDialog()
{
    delete webView;
}

void OverLeaf::LoginDialog::hideScrollBar()
{
    QFile hide_script(":/js/scroll_bar.js");
    hide_script.open(QIODevice::ReadOnly);
    page->runJavaScript(QString(hide_script.readAll()));
}

void OverLeaf::LoginDialog::onCookie(const QNetworkCookie &cookie) 
{
    if (cookie.domain() == OverLeaf::auth_cookie_domain && cookie.name() == OverLeaf::auth_cookie_name) {
        authToken(cookie.value());
        accept();
    }
}


