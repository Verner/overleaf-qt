/*
 * A Qt library for interacting with the OverLeaf web service.
 * Copyright 2017  Jonathan Verner <jonathan@temno.eu>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version
 * accepted by the membership of KDE e.V. (or its successor approved
 * by the membership of KDE e.V.), which shall act as a proxy
 * defined in Section 14 of version 3 of the license.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "project.h"
#include "project_p.h"

#include "api.h"
#include "api_p.h"

#include <QDebug>
#include <QDir>
#include <QJsonObject>
#include <QProcess>

#include <stdio.h>

OverLeaf::Project::Project(const QString& title, OverLeaf::API* api):
    d_ptr(new ProjectPrivate())
{
    d_ptr->api = api;
    d_ptr->title = title;
}

OverLeaf::Project::Project(const QJsonObject& doc, OverLeaf::API *api):
    d_ptr(new ProjectPrivate())
{
    d_ptr->api = api;
    d_ptr->id = doc["id"].toString();
    d_ptr->title = doc["title"].toString();
}

OverLeaf::Project::~Project()
{
    delete d_ptr;
}

QString OverLeaf::Project::id()
{
    return d_ptr->id;
}

QString OverLeaf::Project::title()
{
    return d_ptr->title;
}

void OverLeaf::Project::setTitle(const QString& newTitle)
{
}

QString OverLeaf::Project::gitUrl()
{
    return OverLeaf::git_endpoint+QStringLiteral("/")+d_ptr->id;
}

bool OverLeaf::Project::clone(QString cloneName, QString parentDirectory, bool async)
{
    QProcess *git = new QProcess();
    if (cloneName.length() == 0) cloneName = title().replace(" ","_");
    if (parentDirectory == "") parentDirectory = QDir::currentPath();
    git->setStandardOutputFile("");
    git->setStandardErrorFile("");
    qDebug() << "Cloning " << gitUrl() << "into" << parentDirectory + "/" + cloneName;
    if (! async) {
        git->start("git", QStringList() << "clone" << gitUrl() << parentDirectory+"/"+cloneName );
        if (! git->waitForStarted() )
            return false;
        if (! git->waitForFinished())
            return false;
        bool ret = (git->exitStatus() == QProcess::NormalExit);
        delete git;
        return ret;
    } else {
        connect(git, 
                static_cast<void(QProcess::*)(int, QProcess::ExitStatus)>(&QProcess::finished),
                [this, git](int, QProcess::ExitStatus status) { 
                    git->deleteLater();
                    cloningFinished(status == QProcess::NormalExit);
                });
        git->start("git", QStringList() << "clone" << gitUrl() << parentDirectory+"/"+cloneName );
        return true;
    }
}


