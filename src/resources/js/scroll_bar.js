function addCss(css) {
    var cssElement = document.createElement('style');
    cssElement.innerHTML=css;
    document.body.appendChild(cssElement);
}

var css = "html {overflow: scroll; overflow-x: hidden;}"+
          "::-webkit-scrollbar {width: 0px;  background: transparent;}";

addCss(css);
