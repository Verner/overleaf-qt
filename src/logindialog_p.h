/*
 * A Qt library for interacting with the OverLeaf web service.
 * Copyright 2017  Jonathan Verner <jonathan@temno.eu>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version
 * accepted by the membership of KDE e.V. (or its successor approved
 * by the membership of KDE e.V.), which shall act as a proxy
 * defined in Section 14 of version 3 of the license.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef LOGINDIALOG_P_H
#define LOGINDIALOG_P_H

#include <QDialog>

class QWebEngineView;
class QWebEngineProfile;
class QWebEnginePage;
class QNetworkCookie;

namespace OverLeaf {
    
    class LoginDialog : public QDialog 
    {
        Q_OBJECT
        
    public:
        LoginDialog(QWebEngineProfile* prof = 0, QWidget* parent = 0);
        ~LoginDialog();
        
        int exec();
        
    signals:
        void authToken(const QString &authToken);
        
    private slots:
        void hideScrollBar();
        void onCookie(const QNetworkCookie &cookie);
        
    private:
        QWebEngineView *webView;
        QWebEngineProfile *profile;
        QWebEnginePage *page;    
    };
}

#endif // LOGINDIALOG_P_H

