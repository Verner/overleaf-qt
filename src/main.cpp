/*
 * A Qt library for interacting with the OverLeaf web service.
 * Copyright 2017  Jonathan Verner <jonathan@temno.eu>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version
 * accepted by the membership of KDE e.V. (or its successor approved
 * by the membership of KDE e.V.), which shall act as a proxy
 * defined in Section 14 of version 3 of the license.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "api.h"
#include "project.h"
#include "version.h"

#include <QApplication>
#include <QCommandLineParser>
#include <QDebug>
#include <QWebEngineProfile>

#include <iostream>
#include <list>

const QString APP_NAME = QStringLiteral("OverLeafConnector");
const QString APP_DESCRIPTION = QStringLiteral("A program to interact with the on-line LaTeX editor OverLeaf");
const QString APP_VERSION = QStringLiteral("0.1");
const QString APP_NAMESPACE = QStringLiteral("org.verner");

struct Command {
    std::string name;
    std::string description;
    std::function<int(const QApplication &, QCommandLineParser&)> execute;
};

int loginCommand(const QApplication&, QCommandLineParser &) {
    QWebEngineProfile profile(APP_NAMESPACE+"."+APP_NAME);
    OverLeaf::API api(&profile);
    bool ret = api.login();
    if (ret) {
        std::cout << "AUTH TOKEN: " << api.authToken().toStdString() << std::endl;
    } else {
        std::cerr << "Login Failed" << std::endl;
    }
}

int listProjectsCommand(const QApplication &app, QCommandLineParser &) {
    QWebEngineProfile profile(APP_NAMESPACE+"."+APP_NAME);
    auto api = new OverLeaf::API(&profile);
    if (! api->login() ) {
        std::cerr << "Error: Login Failed" << std::endl;
        return -1;
    }
    QObject::connect( api, &OverLeaf::API::projectsLoaded, [api](){
        int ID_SIZE = 20;
        int TITLE_SIZE = 50;
        std::string separator, header = "ID";
        for(int i=0; i < ID_SIZE-2; i++) {
            header += " ";
            separator += "-";
        }
        header +=    " | TITLE";
        separator += "--------";
        for(int i=0; i < TITLE_SIZE-5; i++) {
            header+= " ";
            separator += "-";
        }
        
        std::cout << separator << std::endl;
        std::cout << header << std::endl;
        std::cout << separator << std::endl;
        
        std::string id, title;
        for( const auto& project : api->projects()) {
            id = project->id().toStdString();
            title = project->title().toStdString();
            for(int i=id.size();i<=ID_SIZE;i++) {
                id += " ";
            }
            std::cout << id <<  title << std::endl;
        }
        
        std::cout << separator << std::endl;
        std::cout << "Total number of projects: " << api->projects().size() << std::endl;
        std::cout << separator << std::endl;
        QApplication::quit();
    });
    api->loadProjects();
    return app.exec();
}

int cloneProjectCommand(const QApplication &app, QCommandLineParser &parser) {
    parser.addPositionalArgument("clone", "Clone the project");
    parser.addPositionalArgument("project_id", "The id of the project to clone");
    parser.addOption(QCommandLineOption("target-dir", "Directory to clone to", "target-dir", ""));
    parser.process(app);
    QString id = parser.positionalArguments().at(1);
    QString target = parser.value("target-dir");
    
    QWebEngineProfile profile(APP_NAMESPACE+"."+APP_NAME);
    OverLeaf::API* api = new OverLeaf::API(&profile);
    if (! api->login() ) {
        std::cerr << "Error: Login Failed" << std::endl;
        return -1;
    }
    
    QObject::connect( api, &OverLeaf::API::projectsLoaded, [api, id, target](){
        OverLeaf::Project *project = api->findProjectById(id);
        if (project) {
            project->clone(target, "", true);
            QObject::connect( project, &OverLeaf::Project::cloningFinished, [api, project](bool ok){
                api->deleteLater();
                if (ok) {
                    QApplication::quit();
                } else {
                    std::cerr << "Error during cloning from" << project->gitUrl().toStdString() << std::endl;
                    project->deleteLater();
                    QApplication::exit(-1);
                }
            });
        } else {
            std::cerr << "Project with id '" << id.toStdString() << "' not found." << std::endl;
            api->deleteLater();
            QApplication::exit(-1);
        }
        QApplication::quit();
    });
    api->loadProjects();
    return app.exec();
}


std::list<Command> commands = { 
    {.name = "login", .description = "login to the OverLeaf service", .execute = loginCommand},
    {.name = "list_projects", .description = "list the logged in users projects", .execute = listProjectsCommand},
    {.name = "clone", .description = "clone the given OverLeaf project", .execute = cloneProjectCommand},
};

void printAvailableCommands() {
    std::cerr << std::endl;
    std::cerr << "Available Commands:" << std::endl;
    std::cerr << std::endl;
    for( const Command &command : commands ) {
        std::cerr << "      " << command.name << "      " << command.description << std::endl;
    }
    std::cerr << std::endl;
}

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    QApplication::setApplicationName(APP_NAME);
    QApplication::setApplicationVersion(APP_VERSION+" (git:"+OverLeaf::GIT_HASH+")");
    
    QCommandLineParser parser;
    parser.setApplicationDescription(APP_DESCRIPTION);
    parser.addHelpOption();
    parser.addVersionOption();
    parser.addPositionalArgument("command", QApplication::translate("main", "Command to execute"));
    
    parser.parse(QCoreApplication::arguments());
    const QStringList args = parser.positionalArguments();
    parser.clearPositionalArguments();
    
    if (args.isEmpty()) {
        std::cerr << "Missing command" << std::endl;
        printAvailableCommands();
        return -1;
    } else {
        for( const auto &command : commands ) {
            if (args.at(0).toStdString() == command.name) {
                return command.execute(app, parser);
            }
        }
        std::cerr << "Invalid command: " << args.at(0).toStdString() << std::endl;
        printAvailableCommands();
        return -1;
    }
    
}

